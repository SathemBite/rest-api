package repository

import java.io.FileWriter

import models.Transfer

import scala.collection.mutable.ArrayBuffer
import scala.io.Source

sealed trait TransferStore {
  def write(transfer: Transfer)

  def getAll(): Seq[Transfer]
}

class FileTransferStore(filePath: String) extends TransferStore {

  override def write(transfer: Transfer): Unit = {
    val fw = new FileWriter(filePath, true)
    fw.write(transfer.toString + "\n")
    fw.close()
  }

  override def getAll(): Seq[Transfer] = {
    Source.fromFile(filePath).getLines.map(Transfer.fromString).toSeq
  }

}

class InMemoryTransferStore(transfers: ArrayBuffer[Transfer]) extends TransferStore {

  override def write(transfer: Transfer): Unit = {
    transfers += transfer
  }

  override def getAll(): Seq[Transfer] = transfers

}

