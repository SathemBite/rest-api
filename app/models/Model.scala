package models

import play.api.libs.json.Json

object Model {

  case class Transfer(amount: String)

  object Implicits {

    implicit val transferFormat = Json.format[Transfer]
  }

}
