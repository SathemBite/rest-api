package models

case class LimitExceededException(msg: String) extends RuntimeException(msg)
