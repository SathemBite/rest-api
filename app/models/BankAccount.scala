package models

import java.time.Instant
import java.time.temporal.ChronoUnit

import scala.collection.mutable.ArrayBuffer
import scala.io.Source

class BankAccount private(
    maxWithdrawalAmount: BigInt,
    maxTxWithdrawalAmount: BigInt,
    maxWithdrawalCount: Int,
    maxDepositAmount: BigInt,
    maxTxDepositAmount: BigInt,
    maxDepositCount: Int,
    deposits: ArrayBuffer[Deposit],
    withdrawals: ArrayBuffer[Withdrawal],
    var accountBalance: BigInt) {


  def deposit(amount: BigInt): Unit = this.synchronized {
    val time = Instant.now().minus(1, ChronoUnit.DAYS)
    while (deposits.headOption.exists(_.timestamp.isBefore(time))) deposits.remove(0)


    val (dayAmount, dayCount) = deposits.foldLeft[(BigInt, Int)]((BigInt(0), 0))((acc, deposit) => (acc._1 + deposit.amount, acc._2 + 1))

    
    if (dayAmount + amount > maxDepositAmount) {
      throw LimitExceededException("Max day deposit amount exceeded")
    }

    if (amount > maxTxDepositAmount) {
      throw LimitExceededException("Max deposit amount per tx exceeded")
    }

    if (dayCount + 1 > maxDepositCount) {
      throw LimitExceededException("Max day deposit count exceeded")
    }

    deposits += Deposit(Instant.now(), amount)

    accountBalance += amount
  }


  def withdraw(amount: BigInt): Unit = this.synchronized {

    val time = Instant.now().minus(1, ChronoUnit.DAYS)
    while (withdrawals.headOption.exists(_.timestamp.isBefore(time))) withdrawals.remove(0)

    val (dayAmount, dayCount) = withdrawals.foldLeft[(BigInt, Int)]((BigInt(0), 0))((acc, withdrawal) => (acc._1 + withdrawal.amount, acc._2 + 1))


    if (dayAmount + amount > maxWithdrawalAmount) {
      throw LimitExceededException("Max day withdrawal amount exceeded")
    }

    if (amount > maxTxWithdrawalAmount) {
      throw LimitExceededException("Max withdrawal amount per tx exceeded")
    }

    if (dayCount + 1 > maxWithdrawalCount) {
      throw LimitExceededException("Max day withdrawal count exceeded")
    }

    withdrawals += Withdrawal(Instant.now(), amount)

    accountBalance -= amount
  }

  def balance(): BigInt = accountBalance

}


object BankAccount {

  lazy val instance = create()

  private def create(): BankAccount = {
    val time = Instant.now().minus(1, ChronoUnit.DAYS)

    var balance = BigInt(0)

    val dayDeposits = ArrayBuffer[Deposit]()
    val dayWithdrawals = ArrayBuffer[Withdrawal]()

    for (line <- Source.fromFile("bank-account").getLines) {
      val transfer = Transfer.fromString(line)

      transfer match {

        case deposit@Deposit(_, amount) =>
          balance += amount
          if (transfer.timestamp.isAfter(time)) {
            dayDeposits +=  deposit
          }

        case withdrawal@Withdrawal(_, amount) =>
          balance -= amount
          if (transfer.timestamp.isAfter(time)) {
            dayWithdrawals +=  withdrawal
          }

      }
    }

    new BankAccount(
      BigInt(50000),
      BigInt(20000),
      3,
      BigInt(150000),
      BigInt(40000),
      4,
      dayDeposits,
      dayWithdrawals,
      balance)

  }
}