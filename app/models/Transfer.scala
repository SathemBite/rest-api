package models

import java.time.Instant

case class Deposit(timestamp: Instant = Instant.now(), amount: BigInt) extends Transfer
case class Withdrawal(timestamp: Instant = Instant.now(), amount: BigInt) extends Transfer

sealed trait Transfer {

  def timestamp: Instant

}

object AsBigInt {
  def unapply(arg: String): Option[BigInt] = Some(BigInt(arg))
}

object AsInstant {
  def unapply(arg: String): Option[Instant] = Some(Instant.parse(arg))
}

object Transfer {

  val DepositReg = "Deposit\\((.*),(\\d+)\\)".r
  val WithdrawalReg = "Withdrawal\\((.*),(\\d+)\\)".r

  def fromString(data: String) = data match {
    case DepositReg(AsInstant(timestamp), AsBigInt(amount)) => Deposit(timestamp, amount)
    case WithdrawalReg(AsInstant(timestamp), AsBigInt(amount)) => Withdrawal(timestamp, amount)
  }

}