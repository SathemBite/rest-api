package routers

import models.{BankAccount}
import play.api.mvc.BodyParsers.parse
import play.api.libs.concurrent.Execution.Implicits._
import models.Model.Implicits._
import models.Model.Transfer
import play.api.libs.iteratee.Enumerator
import play.api.routing.sird._
import play.api.mvc.{Action, ResponseHeader, Result}
import play.api.mvc.Results.{Created, Ok}
import play.api.routing.Router
import repository.{FileTransferStore, TransferStore}

import scala.concurrent.Future

object BankAccountRouter extends FileTransferStore("bank-account") with BankAccountRouter {
  def apply(): Router.Routes = routes
}

trait BankAccountRouter {

  self: TransferStore =>

  def routes: Router.Routes = {

    case GET(p"/balance") => Action.async {
      Future(Ok(BankAccount.instance.balance().toString()))
    }

    case POST(p"/withdraw") => Action.async(parse.json[Transfer]) { implicit request =>
      val transfer = request.body
      Future{
        BankAccount.instance.withdraw(BigInt(transfer.amount))
        Created
      }.recover {
        case exception: Exception => Result(ResponseHeader(400), Enumerator(exception.getMessage.getBytes()))
      }
    }


    case POST(p"/deposit") => Action.async(parse.json[Transfer]) { implicit request =>
      val transfer = request.body
      Future{
        BankAccount.instance.withdraw(BigInt(transfer.amount))
        Created
      }.recover {
        case exception: Exception => Result(ResponseHeader(400), Enumerator(exception.getMessage.getBytes()))
      }
    }

  }

}


