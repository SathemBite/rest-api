**Instructions to run**  

1. Go into project directory and execute in command line: `sbt run`

2. For running tests: `sbt test`

**Endpoints**  
* GET   `/balance`  - returns balance  
    example: `curl http://localhost:9000/balance`  
* POST  `/deposit`  - creates deposit if limits not exceeded  
    example: `curl curl -X POST -H "Content-Type: application/json" --data '{"amount":"10000"}'  localhost:9000/deposit`  
* POST  `/withdraw` - creates withdrawal if limits not exceeded  
    example: `curl -X POST -H "Content-Type: application/json" --data '{"amount":"10000"}'  localhost:9000/withdraw`  