package routers

import models.Model.Implicits._
import models.Model.Transfer
import play.api.libs.json._
import play.api.routing.Router
import play.api.test._

class BankAccountRouterSpec extends BaseRouterSpecification {

  val fakeRouter = Router.from(FakeBankAccountRouter())

  "BankAccount Router" should {

    "Create deposits" in new WithApplicationLoader(fakeAppLoader) {
      val body = Json.toJson(Transfer("10000"))
      val fakePost = FakeRequest(POST, "/deposit").withJsonBody(body)
      val Some(postResult) = route(fakePost)

      status(postResult) must equalTo(CREATED)

    }

    "Returns bad request status when limits is exceeded" in new WithApplicationLoader(fakeAppLoader) {
      val body = Json.toJson(Transfer("10000"))
      val fakePost = FakeRequest(POST, "/deposit").withJsonBody(body)

      route(fakePost)
      route(fakePost)
      route(fakePost)
      route(fakePost)

      val Some(errorResult) = route(fakePost)
      status(errorResult) must equalTo(BAD_REQUEST)
    }

  }

}
