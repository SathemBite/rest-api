package routers

import play.api.ApplicationLoader.Context
import play.api.routing.Router
import play.api.test.PlaySpecification
import play.api.{Application, ApplicationLoader, BuiltInComponentsFromContext}
import repository.FileTransferStore

abstract class BaseRouterSpecification extends PlaySpecification {

  object FakeBankAccountRouter extends FileTransferStore("test-bank-account") with BankAccountRouter {
    def apply(): Router.Routes = routes
  }

  val fakeAppLoader = new ApplicationLoader() {
    def load(context: Context): Application = new BuiltInComponentsFromContext(context) {
      def router = Router.from {
        FakeBankAccountRouter()
      }
    }.application
  }

}
